module Api
	module V1
        class NotesController < ApplicationController   
            #listar todas as notas
            def index
                notes = Note.order('created_at DESC')
                render json: {status: 'SUCCESS', message:'Notas carregados', data:notes},status: :ok
            end
            # listar notas por ID
            def show
                note = Note.find(params[:id])
                render json: {status: 'SUCCESS', message:'Nota Carregada', data:note},status: :ok
            end
            # criar nota
            def create
                note = Note.new(note_params)
                if note.save
                    render json: {status: 'SUCESS', message: 'Criado com sucesso', data:note},status: :ok
                else
                    render json: {status: 'ERROR', message:'Não foi possivel criar esta nota!', data:note.erros},status: :unprocessable_entity
                end
            end
            # deletar nota
            def destroy
                note = Note.find(params[:id])
                note.destroy
                render json: {status: 'SUCCESS', message: "Excluido com sucesso", data:note},status: :ok
            end
            # atualizar nota
            def update
                note = Note.find(params[:id])
                if note.update_attributes(note_params)
                    render json: {status: 'SUCCESS', message:'Updated note', data:note},status: :ok
				else
					render json: {status: 'ERROR', message:'Notes not update', data:note.erros},status: :unprocessable_entity
				end
            end
            #parametros aceitos
            private
            def note_params
                params.permit(:title, :body)
            end
		end
	end
end